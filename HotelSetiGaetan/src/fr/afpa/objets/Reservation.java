package fr.afpa.objets;

import java.time.LocalDate;

public class Reservation {

	Client client;
	private LocalDate dateDebut;
	private LocalDate dateFin;
	private int prixReservation;
	private String[] chambre;
	private int nbReservation;

	public void TableauNbChambreReservees() {

		String[] chambreReservee;

	}

	public Reservation(String client1, LocalDate dateDebut1, LocalDate dateFin1, int prixReservation1,
			String chambre1[]) {
		// Client=client;
		dateDebut = dateDebut1;
		dateFin = dateFin1;
		prixReservation = prixReservation1;
		chambre = chambre1;
		nbReservation = 0;
	}

	/*
	 * public String getClient() { return client; }
	 * 
	 * 
	 * public void setClient(String client1) { client = client1; }
	 */

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut1) {
		dateFin = dateDebut1;
	}

	public LocalDate getDateFin1() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin1) {
		dateFin = dateFin1;
	}

	public int getPrixReservation() {
		return prixReservation;
	}

	public void setPrixReservation(int prixReservation1) {
		prixReservation = prixReservation1;
	}

	public String[] getChambre() {
		return chambre;
	}

	public void setChambre(String[] chambre1) {
		chambre = chambre1;
	}

	public int getNbReservation() {
		return nbReservation;
	}

	public void setNbReservation(int nbReservation1) {
		nbReservation = nbReservation1;
	}

}
