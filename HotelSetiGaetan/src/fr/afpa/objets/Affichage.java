package fr.afpa.objets;

public class Affichage {
/**
 * parcours la liste des chambres du d�but � la fin et verifie si la chambre est � True c'est � dire libre et ensuite affiche la premi�re chambre libre 
 * @param hotel
 */ 
	public static void firstLibre(Hotel hotel) {
		for (int i = 0; i < hotel.getListeChambre().length; i++) {
			if ((hotel.getListeChambre()[i].isChambreLibre() == true)) {
				System.out.println("La premi�re chambre vide est la chambre num�ro : " + (i + 1));
				break;
			}

		}

	}
/**
 *  parcours la liste des chambres de la fin au d�but  et verifie si la chambre est � True c'est � dire libre et ensuite affiche la premi�re chambre libre 
 * @param hotel
 */
	public static void lastLibre(Hotel hotel) {

		for (int i = 64; i >= 0; i--) {
			if ((hotel.getListeChambre()[i].isChambreLibre() == true)) {
				System.out.println("La derni�re chambre vide est la chambre num�ro : " + (i + 1));
				break;
			}

		}
	}
/**
 *  parcours la liste des chambres et verifie si la chambre est � True c'est � dire libre. Pour chaque chambre � True, le nombre nbchambreLibres prend 1. 
 *  On affiche ensuite ce nombre.
 * @param hotel
 */
	public static void nbChambreslibres(Hotel hotel) {
		int nbChambreLibre = 0;
		for (int i = 0; i < hotel.getListeChambre().length; i++) {
			if (hotel.getListeChambre()[i].isChambreLibre() == true) {
				nbChambreLibre = nbChambreLibre + 1;

			}
		}
		System.out.println("Il y a " + nbChambreLibre + " chambres libre(s)");
	}
	 /**
	  *  parcours la liste des chambres et verifie si la chambre est � False c'est � dire r�serv�e. Pour chaque cahmbre � False, le nombre nbChambreReserve prend 1.
	  *  On affiche ensuite ce nombre.
	  * @param hotel
	  */
	public static void nbChambresReservees(Hotel hotel) {
		int nbChambreReserve = 0;
		for (int i = 0; i < hotel.getListeChambre().length; i++) {
			if (hotel.getListeChambre()[i].isChambreLibre() == false) {
				nbChambreReserve = nbChambreReserve + 1;
			}
		}
		System.out.println("Il y a " + nbChambreReserve + " chambres r�serv�e(s)");

	}
/**
 * m�thode qui permet d'afficher l'int�gralit� de la liste de chambre de l'h�tel et les attributs de ces m�mes chambres.
 * i+1 car notre tableau listeChambre commence � 0 hors notre premi�re chambre est la chambre num�ro 1.
 * @param hotel
 */
	public static void etatHotel(Hotel hotel) {
		for (int i = 0; i < hotel.getListeChambre().length; i++) {

			System.out.println(i + 1 + " - " + hotel.getListeChambre()[i]);

		}
	}

}