package fr.afpa.objets;

import java.time.LocalDate;
import java.util.Scanner;

public class Client {

	private String nomClient;
	private String prenomClient;
	private String emailClient;
	private LocalDate dateDeNaissanceClient;
	private String loginClient;

	public Client(String nomClient1, String prenomClient1, String emailClient1, LocalDate dateDeNaissance1,
			String loginClient1) {

		nomClient = nomClient1;
		prenomClient = prenomClient1;
		emailClient = emailClient1;
		dateDeNaissanceClient = dateDeNaissance1;
		setLoginClient(loginClient1);
		// setIdentifiantClient(identifiantClient1);
	}

	public String getNomClient() {
		return nomClient;
	}

	public void setNomClient(String nomClient1) {
		nomClient = nomClient1;
	}

	public String getPrenomClient() {
		return prenomClient;
	}

	public void setPrenomClient(String prenomClient1) {
		prenomClient = prenomClient1;
	}

	public String getEmailClient() {
		return emailClient;
	}

	public void setEmailClient(String emailClient1) {
		emailClient = emailClient1;
	}

	public LocalDate getDateDeNaissanceClient() {
		return dateDeNaissanceClient;
	}

	public void setDateDeNaissanceClient(LocalDate dateDeNaissanceClient1) {
		dateDeNaissanceClient = dateDeNaissanceClient1;
	}

	public String getLoginClient() {
		return loginClient;
	}

	public void setLoginClient(String loginClient1) {
		loginClient = loginClient1;
	}

	@Override
	public String toString() {
		return "Client [nomClient=" + nomClient + ", prenomClient=" + prenomClient + ", emailClient=" + emailClient
				+ ", dateDeNaissanceClient=" + dateDeNaissanceClient + ", loginClient=" + loginClient + "]";
	}

}
