package fr.afpa.objets;

public class Chambre {

	private String typeChambre;
	private String taille;
	private String vues;
	private String tarif;
	private String options;
	private Reservation[] nbResaChambre;
	private boolean chambreLibre;

	/**
	 * 
	 * @param type : le type de chambre 
	 * @param surface : la surface de la chambre 
	 * @param panorama : le panorama de la chambre 
	 * @param prix : le prix de la chambre 
	 * @param choixOptions : le choix des options de la chambre
	 * @param chambreLibre1 : la chambre est libre
	 * @param nbResaChambre1 : le total des chambres libres 
	 */
	public Chambre(String type, String surface, String panorama, String prix, String choixOptions,
			boolean chambreLibre1, Reservation[] nbResaChambre1) {
		typeChambre = type;
		taille = surface;
		vues = panorama;
		tarif = prix;
		options = choixOptions;
		chambreLibre = chambreLibre1;
		nbResaChambre = nbResaChambre1;

	}
/**
 * le getter de Chambre 
 * @return type de chambre 
 */
	public String getTypeChambre() {
		return typeChambre;
	}

	public void setTypeChambre(String type) {
		typeChambre = type;
	}

	public String getTaille() {
		return taille;
	}

	public void setTaille(String surface) {
		taille = surface;
	}

	public String getVues() {
		return vues;
	}

	public void setVues(String panorama) {
		vues = panorama;
	}

	public String getTarif() {
		return tarif;
	}

	public void setTarif(String prix) {
		tarif = prix;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String choixOptions) {
		options = choixOptions;
	}

	public boolean isChambreLibre() {
		return chambreLibre;
	}

	public void setChambreLibre(boolean chambreLibre1) {
		chambreLibre = chambreLibre1;
	}

	@Override
	public String toString() {
		return "Chambre [typeChambre=" + typeChambre + ", taille=" + taille + ", vues=" + vues + ", tarif=" + tarif
				+ ", options=" + options + ", chambreLibre=" + chambreLibre + "]";
	}

	public Reservation[] getNbResaChambre() {
		return nbResaChambre;
	}

	public void setNbResaChambre(Reservation[] nbResaChambre1) {
		nbResaChambre = nbResaChambre1;
	}

}
