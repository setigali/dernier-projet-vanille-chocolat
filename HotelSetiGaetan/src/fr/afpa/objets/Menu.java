package fr.afpa.objets;

import fr.afpa.objets.Controle;
import java.util.Scanner;
import fr.afpa.objets.Chambre;

public class Menu {
	Scanner in = new Scanner(System.in);

	public void affichageMenu(Hotel hotel) {

		char option = ' ';
		System.out.println("option");
		while (option != 'Q') {

			System.out.println(
					"-------------------------------   MENU HOTEL CDA CHOCOLAT-VANILLE   -------------------------------------");
			System.out.println();
			System.out.println("A- Afficher l��tat de l�h�tel");
			System.out.println("B- Afficher le nombre de chambres r�serv�es");
			System.out.println("C- Afficher le nombre de chambres libres");
			System.out.println("D- Afficher le num�ro de la premi�re chambre vide");
			System.out.println("E- Afficher le num�ro de la derni�re chambre vide");
			System.out.println("F- R�server une chambre ");
			System.out.println("G- Lib�rer une chambre ");
			System.out.println("H- Modifier une r�servation");
			System.out.println("I- Annuler une r�servation");
			System.out.println();
			System.out.println("Q- Quitter");
			System.out.println();
			System.out.println(
					"---------------------------------------------------------------------------------------------------------");
			System.out.println();
			System.out.println("Votre choix:");

			Scanner in = new Scanner(System.in);

			option = in.next().toUpperCase().charAt(0);

			switch (option) {

			case 'A':
				Affichage.etatHotel(hotel);

				break;
			case 'B':

				Affichage.nbChambresReservees(hotel);
				break;
			case 'C':
				Affichage.nbChambreslibres(hotel);
				break;
			case 'D':
				Affichage.firstLibre(hotel);
				break;
			case 'E':
				Affichage.lastLibre(hotel);
				break;
			case 'F':
				Controle code = new Controle();
				code.validLoginAdmin();
				hotel.reserver();

				break;
			case 'G':
				Controle code1 = new Controle();
				code1.validLoginAdmin();
				hotel.liberer();
				break;
			case 'H':
				Controle code2 = new Controle();
				code2.validLoginAdmin();
				hotel.modifier();

				break;
			case 'I':
				Controle code3 = new Controle();
				code3.validLoginAdmin();
				hotel.annuler();
				break;
			case 'Q':
				break;
			default:
				System.out.println("Veuillez saisir un caract�re valide.");
				break;

			}
		}
	}

}
