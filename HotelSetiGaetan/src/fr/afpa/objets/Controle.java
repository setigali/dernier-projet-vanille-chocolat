package fr.afpa.objets;

import java.time.LocalDate;
import java.util.Scanner;

public class Controle {

	Scanner in = new Scanner(System.in);

	private String CodeClient;
	private String CodeEmploye;
	private String CodeAdmin = "SETI59";
	private static String CodeCarte;

	public String getCodeClient() {
		return CodeClient;
	}

	public void setCodeClient(String codeClient) {
		CodeClient = codeClient;
	}

	public String getCodeEmploye() {
		return CodeEmploye;
	}

	public void setCodeEmploye(String codeEmploye) {
		CodeEmploye = codeEmploye;
	}

	public String getCodeAdmin() {
		return CodeAdmin;
	}

	public void setCodeAdmin(String codeAdmin) {
		CodeAdmin = codeAdmin;
	}

	public String getCodeCarte() {
		return CodeCarte;
	}

	public static void setCodeCarte(String codeCarte) {
		CodeCarte = codeCarte;
	}

	
	/**
	 * Controle de la conformit� du login admin SETI59
	 */
	public void validLoginAdmin() {

		String loginAdmin = "";
		System.out.println("Veuillez saisir votre login Administrateur");
		loginAdmin = in.nextLine();
		if (loginAdmin.contentEquals(CodeAdmin)) {
			System.out.println("Code valide");
			setCodeAdmin(loginAdmin);
		}

		else {
			System.out.println("Code �ronn� ! Veuillez saisir un nouveau code");
			loginAdmin = in.nextLine();
		}

	}

	/**
	 * Permet de choisir son profil au moment du lancement du programme. 
	 */
	public void choixProfil() {
		System.out.println("Veuillez choisir votre profil: Employ� / Client");
		String choix = " ";
		System.out.println("1 - Client");
		System.out.println("2 - Employ�");
		Scanner in = new Scanner(System.in);
		choix = in.nextLine();

		switch (choix) {

		case "1":
			validLoginClient();
			break;
		case "2":
			validLoginEmploye();
			break;
		default:
			System.out.println("Erreur !");
			choixProfil();
			break;

		}

	}

	/**
	 * Verification du code de la carte bancaire 
	 */
	public static void codeCB() {
		Scanner in = new Scanner(System.in);
		System.out.println("Veuillez saisir votre num�ro de carte bancaire");
		String cb = in.nextLine();
		boolean b = false;

		while (b == false) {
			if (cb.length() == 10) {  // on s'assure que la saisie est �gale � 10 caract�res
				for (int i = 0; i < cb.length(); i++) { // on parcourt la saisie
					if (Character.isDigit(cb.charAt(i))) { // on v�rifie que chaque caract�re de la saisie est un chiffre
						b = true;
					} else {
						b = false;
						System.out.println("Num�ro de carte invalide, veuillez retaper le num�ro"); // s'affiche lors d'une mauvaise saisie
						cb = in.nextLine();
						break;
					}
				}
				if (b) {
					setCodeCarte(cb);
				}
			} else {
				System.out.println("Num�ro de carte invalide, veuillez retaper le num�ro"); 
				cb = in.nextLine();
			}
		}
	}

	public void validLoginClient() { // validation du code Client selon les r�gles m�tiers

		System.out.println("Veuillez saisir votre login Client");
		String loginClient = in.nextLine();
		boolean b = false;

		while (b == false) {
			if (loginClient.length() == 10) {
				for (int i = 0; i < loginClient.length(); i++) {
					if (Character.isDigit(loginClient.charAt(i))) {
						b = true;
					} else {
						b = false;
						System.out.println("Code invalide, veuillez retaper un code");
						loginClient = in.nextLine();
						break;
					}
				}
				if (b) {
					setCodeClient(loginClient);
				}
			} else {
				System.out.println("Code invalide, veuillez retaper un code");
				loginClient = in.nextLine();
			}
		}
	}

	public void validLoginEmploye() {
		Scanner in = new Scanner(System.in);
		System.out.println("Veuillez saisir votre login Employ�");
		String loginEmploye = in.nextLine();
		boolean maj = false;
		boolean chiffre = false;

		while (maj == false || chiffre == false) {
			if (loginEmploye.length() == 5) { // on s'assure que la saisie est �gale � 5 caract�res
				for (int i = 0; i < 2; i++) { // on parcourt les deux premiers caract�res de la saisie
					if (loginEmploye.charAt(0) == 71 && loginEmploye.charAt(1) == 72) { // on v�rifie que ces deux premiers caract�res correspondent � G et H (dans cet ordre et en majuscule) en utilisant le code ASCII de chacun
						maj = true;
					}

					else {
						maj = false;
						break;
					}
				}
				for (int i = 2; i < loginEmploye.length(); i++) { // on parcourt la suite de la sasie , on commence donc � 2 
					if (Character.isDigit(loginEmploye.charAt(i))) { // on s'assure que le reste de la saisie correspond bien � 3 chiffres
						chiffre = true;
					} else {
						chiffre = false;
						break;
					}
				}

				if (maj && chiffre) {
					setCodeEmploye(loginEmploye);
				} else {
					System.out.println("Code invalide, veuillez retaper un code");
					loginEmploye = in.nextLine();
				}
			}

			else {
				System.out.println("Code invalide, veuillez retaper un code");
				loginEmploye = in.nextLine();
			}
		}
	}

	public static LocalDate formatDateNaissance() {
		String dateDeNaissanceClient = "";
		Scanner in = new Scanner(System.in);
		try {

			while (dateDeNaissanceClient.length() != 10) {
				System.out.println("Veuillez saisir une date de naissance au bon format.");
				dateDeNaissanceClient = in.nextLine();
				String[] donneesDate = dateDeNaissanceClient.split("/");
			}
			if (dateDeNaissanceClient.length() > 9 && dateDeNaissanceClient.length() < 11) {
				String[] donneesDate = dateDeNaissanceClient.split("/");

				LocalDate date = LocalDate.parse(donneesDate[2] + "-" + donneesDate[1] + "-" + donneesDate[0]);

			}
		} catch (Exception DateTimeParseException) {
			System.out.println("Veuillez saisir une date de naissance au bon format.");
			dateDeNaissanceClient = in.nextLine();
			String[] donneesDate = dateDeNaissanceClient.split("/");
		}
		return LocalDate.parse(dateDeNaissanceClient);
	}

	public static LocalDate formatDateDebut() {

		LocalDate aujourdhui = LocalDate.now();
		String dateDebut = "";
		LocalDate date = null;
		String[] donneesDate;
		Scanner in = new Scanner(System.in);
		boolean dateCorrecte = false;

		while (dateDebut.length() != 10 || date.isBefore(aujourdhui)) {
			System.out.println("Veuillez saisir la date de debut de la r�servation du client : JJ/MM/AAAA ");
			dateDebut = in.nextLine();
			donneesDate = dateDebut.split("/");
			try {

				// System.out.println("Veuillez saisir une date de d�but au bon format.");
				// dateDebut = in.nextLine();
				// donneesDate = dateDebut.split("/");

				// date = LocalDate.parse(donneesDate[2]+"-"+donneesDate[1]+"-"+donneesDate[0]);

				if (dateDebut.length() > 9 && dateDebut.length() < 11) {
					donneesDate = dateDebut.split("/");

					date = LocalDate.parse(donneesDate[2] + "-" + donneesDate[1] + "-" + donneesDate[0]);

				}
			} catch (Exception DateTimeParseException) {
				System.out.println("Veuillez saisir une date de d�but au bon format.");
				dateDebut = in.nextLine();
				donneesDate = dateDebut.split("/");
			}
		}
		return date;

	}

	public static LocalDate formatDateFin(LocalDate date) {
		LocalDate aujourdhui = LocalDate.now();
		String dateFin = "";
		LocalDate dateFin2 = null;
		String[] donneesDate;
		Scanner in = new Scanner(System.in);

		while (dateFin.length() != 10 || dateFin2.isBefore(aujourdhui)) {
			System.out.println("Veuillez saisir la date de fin de la r�servation du client : JJ/MM/AAAA");
			dateFin = in.nextLine();
			donneesDate = dateFin.split("/");
			try {

				// System.out.println(" ");
				// dateFin = in.nextLine();

				if (dateFin.length() > 9 && dateFin.length() < 11) {
					donneesDate = dateFin.split("/");

					dateFin2 = LocalDate.parse(donneesDate[2] + "-" + donneesDate[1] + "-" + donneesDate[0]);

				}
			} catch (Exception DateTimeParseException) {
				System.out.println("Veuillez saisir une date de fin au bon format.");
				dateFin = in.nextLine();
				donneesDate = dateFin.split("/");
			}

		}
		return dateFin2;
	}

}
