package fr.afpa.objets;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

import fr.afpa.objets.Chambre;

public class Hotel {
	Scanner in = new Scanner(System.in);
	private Chambre[] listeChambre;

	public Hotel() {

		ListeDeChambre();
		gestionHotel();
	}

	public Chambre[] getListeChambre() {
		return listeChambre;
	}

	public void setListeChambre(Chambre[] listeChambre1) {
		listeChambre = listeChambre1;
	}

	private void gestionHotel() {
		Controle controle = new Controle();
		controle.choixProfil();
		Menu menu = new Menu();
		menu.affichageMenu(this);

	}

	public void ListeDeChambre() {
		String[] chambre = null;

		try {
			FileReader lecteur = new FileReader(
					"C:\\ENV\\Workspace\\dernier-projet-vanille-chocolat\\ListeChambres_V3.csv");

			// FileReader lecteur = new
			// FileReader("C:\\Users\\59013-70-15\\Desktop\\ListeChambres_V3.csv");
			// ("C:\\Users\\setia\\Desktop\\Projets CDA\\dernier projet vanille
			// chocolat\\dernier-projet-vanille-chocolat\\ListeChambres_V3.csv");

			// FileReader lecteur = new FileReader("C:\\Users\\setia\\Desktop\\Projets
			// CDA\\dernier projet vanille
			// chocolat\\dernier-projet-vanille-chocolat\\ListeChambres_V3.csv");

			BufferedReader abc = new BufferedReader(lecteur);
			int sommeChambre = 0;
			int chiffreAffaire = 0;
			String descriptionChambre = "";
			if (abc.ready()) {
				chambre = abc.readLine().split(";");
			}
			while (abc.ready()) {
				chambre = abc.readLine().split(";");

				sommeChambre += Integer.parseInt(chambre[5]);

			}
			System.out.println("Il y a " + sommeChambre + " chambre(s) libre(s) dans l'h�tel : ");
			System.out.println();

			abc.close();

			listeChambre = new Chambre[sommeChambre];
			int indiceChambre = 0;

			// ("C:\\Users\\setia\\Desktop\\Developpement\\Workspace\\Last Projet Vanille
			// Chocolat\\ListeChambres_V3.csv");
			FileReader lecteur2 = new FileReader(
					"C:\\ENV\\Workspace\\dernier-projet-vanille-chocolat\\ListeChambres_V3.csv");

			// FileReader lecteur2 = new
			// FileReader("C:\\Users\\setia\\Desktop\\Developpement\\Workspace\\Last Projet
			// Vanille Chocolat\\ListeChambres_V3.csv");
			// FileReader lecteur2 = new
			// FileReader("C:\\Users\\59013-70-07\\Downloads\\ListeChambres_V3.csv");

			BufferedReader abc2 = new BufferedReader(lecteur2);
			if (abc2.ready()) {
				chambre = abc2.readLine().split(";");
			}
			while (abc2.ready()) {
				chambre = abc2.readLine().split(";");

				for (int i = 0; i < Integer.parseInt(chambre[5]); i++) {

					listeChambre[indiceChambre] = new Chambre(chambre[0], chambre[1], chambre[2], chambre[4],
							chambre[6], true, null);

					indiceChambre++;
				}
			}
			for (int i = 0; i < listeChambre.length; i++) {
				System.out.println(i + 1 + " - " + listeChambre[i]);

			}
			abc2.close();

		}

		catch (Exception e) {
			System.out.println("Erreur" + e);
		}
	}

	public void liberer() {
		Scanner in = new Scanner(System.in);
		int numliberation;
		System.out.println("Quelle chambre voulez-vous liberer ?");
		System.out.println("Veuillez saisir le num�ro de la chambre.");
		numliberation = in.nextInt();

		for (int i = 0; i < getListeChambre().length; i++) {
			if (numliberation >= 1 && numliberation <= 65) {

				getListeChambre()[numliberation - 1].setChambreLibre(true);
				// nbChambreReserve=nbChambreReserve-1;

			}
		}
		System.out.println("La chambre " + numliberation + " est liber�e.");
	}

	public void reserver() {

		Scanner in = new Scanner(System.in);

		// int nbChambreReserve=0;
		LocalDate dateDebut;
		LocalDate dateFin;
		dateDebut = Controle.formatDateDebut();
		dateFin = Controle.formatDateFin(dateDebut);
		System.out.println("Quelle chambre voulez-vous reserver ?");
		System.out.println("Veuillez saisir le num�ro de la chambre.");
		int numreservation = in.nextInt();

		int duree = Period.between(dateDebut, dateFin).getDays();
		String tarif = getListeChambre()[numreservation - 1].getTarif();
		int totalApayer = duree * Integer.parseInt(tarif);
		String cb;

		 Client client = null;

		for (int i = 0; i <= getListeChambre().length; i++) {
			if (numreservation >= 1 && numreservation <= 65) {

				getListeChambre()[numreservation - 1].setChambreLibre(false);

				creationCompteClient();
				break;

			}
		}

		System.out.println("La chambre " + numreservation + " est reserv�e.");

		for (int i = 0; i < getListeChambre().length; i++) {
			System.out.println(
					"Votre num�ro de chambre est: " + numreservation + "\r\n" + "La p�riode de votre s�jour est: "
							+ " du " + dateDebut + " au " + dateFin + "\r\n" + getListeChambre()[numreservation - 1]
							+ "\r\n" + "Il vous faut payer: " + (duree * Integer.parseInt(tarif) + " euros."));
			break;
		}

		Controle.codeCB();
		Facture.facturer(client);
		//
		// Facture.facturer(client);

	}

	public Client creationCompteClient() {

		System.out.println("Vous allez cr�er un compte client");
		System.out.println("Veuillez saisir le nom du client");
		String nomClient = in.nextLine();
		System.out.println("Veuillez saisir le pr�nom du client");
		String prenomClient = in.nextLine();
		// System.out.println("Veuillez saisir votre date de naissance client");
//verifier la date de naissance
		// LocalDate dateDeNaissanceClient = //nouvelle date
		System.out.println("Veuillez saisir l'adresse email du client");
		String emailClient = in.nextLine();
		System.out.println("Veuillez saisir votre login client");
		String loginClient = in.nextLine();
		System.out.println("Voici le r�capitulatif de votre r�servation: ");
		System.out.println("Nom: " + nomClient + "\r\n" + "Pr�nom: " + prenomClient + "\r\n" + "Email: " + emailClient);

		return new Client(nomClient, prenomClient, emailClient, null, loginClient);
	}

	public void annuler() {

		Scanner in = new Scanner(System.in);

		// int nbChambreReserve=0;
		LocalDate dateDebut;
		LocalDate dateFin;
		dateDebut = Controle.formatDateDebut();
		dateFin = Controle.formatDateFin(dateDebut);

		System.out.println("Quelle chambre voulez-vous reserver ?");
		System.out.println("Veuillez saisir le num�ro de la chambre.");
		int numreservation = in.nextInt();

		int duree = Period.between(dateDebut, dateFin).getDays();
		String tarif = getListeChambre()[numreservation].getTarif();
		

		Client client = null ;

		for (int i = 0; i < getListeChambre().length; i++) {
			if (numreservation >= 1 && numreservation <= 65) {

				getListeChambre()[numreservation].setChambreLibre(true);

				// verification creationCompteClient() et compte client existant ;break;

			}
		}

		System.out.println("La reservation de la chambre " + numreservation + " est annul�e.");

		for (int i = 0; i < getListeChambre().length; i++) {
			System.out.println(
					"Votre num�ro de chambre est: " + numreservation + "\r\n" + "La p�riode de votre s�jour est: "
							+ " du " + dateDebut + " au " + dateFin + "\r\n" + getListeChambre()[numreservation - 1]
							+ "\r\n" + "Nous vous rembouserons : " + (duree * Integer.parseInt(tarif) + " euros."));
			break;
		}
		Controle.codeCB();
		
	}

	public void modifier() {

	}
}
