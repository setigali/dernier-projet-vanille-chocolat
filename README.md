Description du projet 
Nom du projet : projet Vanille - Chocolat 
Il s'agit d'un programme qui permet la gestion des reservations de chambres d'un hotel. Le programme permet de reserver une ou plusieurs chambres(chambres num�rot�es de 1 � 65) � une ou plusieurs dates donn�es.
Il est aussi possible apr�s avoir fait une ou plusieurs reservations de modifier et ou d'annuler la ou  les reservations. A la fn de chaque operation, le programme doit envoye un mail au client recapitulant l'operation et les facture de payement ou remboursement correspondant � l'operation en format pdf.
Le client peut �galement consulter ses reservations en se connectant � son compte avec son login. 

1- Pr�-requis

- Un ordinateur 
- Telecharger le fichier ListeChambre_V3.csv

2- Installation

- Eclipse : Pour lancer le programme vous aurez besoin d'un environement de developpement tel que Eclipse(� telecharger sur le site d'Oracle). Veuillez � biien verifier la version compatible avec votre ordinateur. 

- Invite de commande : Si vous ne disposez pas d'un environement de developpement et que vous ne souhaitez pas en installer un, vous pouvez utiliser l'Invite de commande de votre ordinateur. 


3- D�marrage

Quelques login necessaires :
- login Client (profil client) : 3 chiffres
- login Client (profil employ�) : 10 chiffres 
- login employ� : GH suivi de 3 chiffres 
- login administrateur : SETI59

Au lancement du programme il faut choisir son profil : client ou employ�. 

Cas du client :
Il faudra saisir le login client compos� de 3 chiffres. 
Si le client a une ou plusieurs reservations, le programme lui affichera ses reservations pendant 20 secondes.

Cas de l'employ� : 
Il faudra saisir le login employ� et ensuite, l'employ� peut effectuer gr�ce � son login administrateur, tous les cas affich�s dans le Menu. Dans la version actuelle du programme les options disponnibles vont de A � G. Les autres n'ont pas encore �t� developp�es. 
Pour quitter le Menu, il faudra choisir l'option Q sinon le Menu s'affiche en continu. 

4- Fabriqu� avec 

- Eclipse

biblioth�ques � importer : 
io-7.1.8.jar /
kernel-7.1.8.jar / 
layout-7.1.8.jar / 
log4j-1.2.17.jar /
slf4j-api-1.7.18.jar / 
slf4j-log4j12-1.7.18.jar / 
javax.mail.jar / 

- Itext
- Gitbucket 
- GitKraken


5- Auteurs 

- Ga�tan Delacroix
- Seti Gali 
